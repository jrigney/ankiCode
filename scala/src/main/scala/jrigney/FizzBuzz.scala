package jrigney

object FizzBuzz extends App {

  def fizzbuzz1(x: Int): String = {

    x match {
      case x if x % 3 == 0 && x % 5 == 0 => "fizzbuzz"
      case x if x % 5 == 0 => "buzz"
      case x if x % 3 == 0 => "fizz"
      case _ => x.toString
    }

  }

  def fizzbuzz2(x: Int): String = {

    (x % 3, x % 5) match {
      case (0, 0) => "fizzbuzz"
      case (_, 0) => "buzz"
      case (0, _) => "fizz"
      case _ => x.toString
    }

  }

  def fizzbuzz3(x: Int): String = {
    sealed trait FB
    final case object Fizzbuzz extends FB
    final case class Buzz(x:Int) extends FB
    final case class Fizz(x:Int) extends FB
    final case class Number(x: Int) extends FB

    val fizz: (FB) => FB = (x: FB) => {
      x match {
        case Number(x) if x % 3 == 0 => Fizz(x)
        case Buzz(x) if x % 3 == 0 => Fizzbuzz
        case _ => x
      }
    }

    val buzz: (FB) => FB = (x: FB) => {
      x match {
        case Number(x) if x % 5 == 0 => Buzz(x)
        case a@Fizz(x) => if (x % 5 == 0) Fizzbuzz else a
        case _ => x
      }
    }

    val fizzbuzzer: FB => FB = buzz compose fizz

    fizzbuzzer(Number(x)) match {
      case Fizzbuzz => "fizzbuzz"
      case Buzz(_) => "buzz"
      case Fizz(_)=> "fizz"
      case Number(x) => x.toString
    }

  }

  val fizzbuzz = fizzbuzz3 _
  println(s" ${(1 to 16).map(fizzbuzz).mkString(",")}")
}
