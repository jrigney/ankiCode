package jrigney

import scala.annotation.tailrec
import scala.collection.immutable.VectorBuilder

object BinarySearchTree {

  def inorderTraverseTailRec[A](binaryTree: BinaryTree[A]): List[A] = {

    @tailrec
    def loop(stack: List[BinaryTree[A]], acc: List[A]): List[A] = {

      stack match {
        case Nil => acc.reverse
        case Leaf(value) +: tail => loop(tail, value +: acc)
        case Branch(left, value, right) +: tail =>
          val list: List[BinaryTree[A]] = left :: Leaf(value) :: right :: tail
          loop(list, acc)
        case NoLeaf +: tail => loop(tail, acc)
      }

    }

    loop(List(binaryTree), List.empty)
  }

  def foldLeft[A, B](init: B)(op: (B, A) => B)(binaryTree: BinaryTree[A]): B = {

    @tailrec
    def loop(stack: List[BinaryTree[A]], acc: B): B = {

      stack match {
        case Nil => acc
        case Leaf(value) +: tail => loop(tail, op(acc,value))
        case Branch(left, value, right) +: tail => loop(left +: Leaf(value) +: right +: tail, acc)
        case NoLeaf +: tail => loop(tail, acc)
      }

    }

    loop(List(binaryTree), init)
  }

  def inorderTraversalNonTailRec[A](binaryTree: BinaryTree[A]): Vector[A] = {

    val builder = new VectorBuilder[A]()

    //not tailrec
    def loop(binaryTree: BinaryTree[A]): Unit = {

      binaryTree match {
        case NoLeaf => ()
        case Leaf(value) => builder += value
        case Branch(left, value, right) =>
          loop(left)
          builder += value
          loop(right)
      }
    }

    loop(binaryTree)
    builder.result()
  }

  def levelorderTraversal[A](binaryTree: BinaryTree[A]):List[A] = {

    @tailrec
    def loop(queue: Vector[BinaryTree[A]], acc:List[A]):List[A] = {
      queue match {
        case Leaf(value) +: tail => loop(tail, value +: acc)
        case Branch(left,value,right) +: tail => loop(tail :+ left :+ right, value +: acc)
        case NoLeaf +: tail => loop(tail,acc)
        case x if(x.isEmpty) => acc.reverse
      }
    }
    loop(Vector(binaryTree), List.empty)
  }
}

sealed trait BinaryTree[+A]

case class Leaf[A](value: A) extends BinaryTree[A]

case object NoLeaf extends BinaryTree[Nothing]

case class Branch[A](left: BinaryTree[A], value: A, right: BinaryTree[A]) extends BinaryTree[A]
