package jrigney

import org.scalatest.{Matchers, WordSpec}


class MatrixOpsTest extends WordSpec with Matchers {

  "A matrix" can {
    "with two filled cells" should {
      "return true if the cells are diagonal to each other" in {
        MatrixOps.isDiagonal((0,0),(1,1)) shouldBe(true)
        MatrixOps.isDiagonal((2,2),(4,0)) shouldBe(true)
        MatrixOps.isDiagonal((2,2),(0,4)) shouldBe(true)
        MatrixOps.isDiagonal((0,0),(2,2)) shouldBe(true)
        MatrixOps.isDiagonal((4,4),(2,2)) shouldBe(true)
      }
    }
  }
}

object MatrixOpsTest {

  type Matrix[A] = Vector[Vector[A]]

  val twoBy: Matrix[Int] = Vector(
    Vector.fill[Int](2)(0),
    Vector.fill[Int](2)(0))
}