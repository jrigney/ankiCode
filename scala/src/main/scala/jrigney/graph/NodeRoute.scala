package jrigney.graph

import scala.annotation.tailrec

object NodeRoute extends App{

  def nodeRoute(g: Map[Int, List[Int]], start: Int, target: Int): Boolean = {

    @tailrec
    def loop( fringe: List[Int], visited: List[Int]): Boolean = {
      fringe match {
        case Nil => false
        case h +: t =>
          if (h == target) true
          else {
            val potentialNeighbors:List[Int] = fringe.flatMap(g)
            val newNeighbors:List[Int] = potentialNeighbors.filterNot(visited.contains)
            if (newNeighbors.isEmpty) loop(t, h+:visited)
            else loop(t ++ newNeighbors, h+:visited)
          }
      }
    }

    loop(g(start), List.empty)
  }

  val g = Map(
    (1 -> List(2,3)),
    (2 -> List.empty),
    (3 -> List(4)),
    (4 -> List.empty)
  )

  println(s"${nodeRoute(g,1,4)}")
  println(s"${nodeRoute(g,3,4)}")
  println(s"${nodeRoute(g,2,4)}")
  println(s"${nodeRoute(g,2,5)}")

  val g2 = Map(
    (1 -> List(2,3)),
    (2 -> List(6)),
    (3 -> List(4,5)),
    (4 -> List.empty),
    (5 -> List.empty),
    (6 -> List(4))
  )

  println(s"${nodeRoute(g2,3,4)}")
  println(s"${nodeRoute(g2,3,6)}")
  println(s"${nodeRoute(g2,2,4)}")



}
