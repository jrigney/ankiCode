package jrigney

import org.scalatest.{FunSuite, Matchers}

class FactorsTest extends FunSuite with Matchers {

  test("factors") {
    Factors.factors(28).sorted shouldBe(List(1,2,4,7,14, 28))
    Factors.factors(1).sorted shouldBe(List(1))
    Factors.factors(2).sorted shouldBe(List(1,2))
    Factors.factors(335503).sorted shouldBe(List(1, 7, 41, 49, 167, 287, 1169, 2009, 6847, 8183, 47929, 335503))
  }

}
