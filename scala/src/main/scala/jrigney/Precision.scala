package jrigney

object Precision extends App {
  def precision(x: Int, y: Int, places: Int) = {

    val (_, _, p) = (1 to places).foldLeft((x % y * 10, y, Vector.empty[Int]))({ case ((x, y, acc), _) =>
      val num = x / y
      val rem = x % y
      (rem * 10, y, acc :+ num)
    })

    val beforeDecimal = x / y

    (beforeDecimal, p)
  }

    println(s"${precision(12, 7, 5)}")
  //  println(s"${precision(1, 3, 3)}")

  def toInt(xs: Vector[Int]): Int = {

    val (_, result) = xs.foldRight((0, 0)) { case (next, (i, acc)) =>
      val nAcc = (acc + Math.pow(10, i) * next).toInt
      (i + 1, nAcc)
    }
    result
  }

  //  println(s"toInt [${toInt(Vector(7,1,4,2,8))}]")
  //  println(s"toInt [${toInt(Vector(3,3,3))}]")

}
