package jrigney

import org.scalatest.{Matchers, WordSpec}

class BinarySearchTreeTest extends WordSpec with Matchers {

  import BinarySearchTreeTest._
  "A Tree" can {

    "for a binary tree" should {

      "inorder traversal" in {
        BinarySearchTree.inorderTraverseTailRec(notCompleteBinaryTree) shouldBe List(3,5,7,10,20,30)
      }

      "inorder foldLeft" in {
        val fn:(Vector[Int],Int) => Vector[Int] = (acc:Vector[Int],value:Int) => { acc :+ value }

        BinarySearchTree.foldLeft(Vector.empty[Int]){fn} (notCompleteBinaryTree) shouldBe List(3,5,7,10,20,30)
      }

      "inorder notTailrec" in {
        BinarySearchTree.inorderTraversalNonTailRec(notCompleteBinaryTree) shouldBe List(3,5,7,10,20,30)
      }

      "levelorder tailrec" in {
        BinarySearchTree.levelorderTraversal(notCompleteBinaryTree) shouldBe List(10,5,20,3,7,30)
      }

    }
  }

}

object BinarySearchTreeTest {

  val notCompleteBinaryTree: BinaryTree[Int] = Branch(
    Branch(Leaf(3), 5, Leaf(7))
    , 10
    , Branch(NoLeaf, 20, Leaf(30)))
}


