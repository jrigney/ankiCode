package jrigney

import scala.annotation.tailrec

object Hanoi extends App {

  def hanoiTailRecursive(n: Int): Unit = {

    case class Pos(n: Int, originPeg: Int, destinationPeg: Int, bufferPeg: Int)

    @tailrec
    def loop(pos: Pos, rest: List[Pos]): Unit = {

            println(s"pos [$pos] rest [$rest]")

      val Pos(numDisks, origin, destination, buffer) = pos

      if (numDisks == 1) {
        println(s"Move disk from pole $origin to pole $destination")
        rest match {
          case Nil => (): Unit
          case h +: t => loop(h, t)
        }
      } else {

        val pos1 = Pos(numDisks - 1, origin, buffer, destination) //Moving n-1 disks from origin to buffer
        val pos2 = Pos(1, origin, destination, buffer) //Moving final disk from origin to destination
        val pos3 = Pos(numDisks - 1, buffer, destination, origin) // Moving n-1 disks from buffer to destination

        //rest (pos2 :: pos3 :: rest) is the stack instead so we need to add the same things we would add in the
        // non-tail recursive version.
        loop(pos1, pos2 :: pos3 :: rest)
      }
    }

    loop(Pos(n, 1, 2, 3), List.empty)

  }

  def hanoiNonTailRecursive(n: Int): Unit = {

    def loop(n: Int, originPeg: Int, destinationPeg: Int, bufferPeg: Int): Unit = { // Recursive Function
      if (n == 1) {
        Console.println("Move disk from peg " + originPeg + " to peg " + destinationPeg) // each move iteration printed.
      }
      else {
        loop(n - 1, originPeg, bufferPeg, destinationPeg) //Moving n-1 disks from source to Intermediate
        loop(1, originPeg, destinationPeg, bufferPeg) // Printing all the move iterations
        loop(n - 1, bufferPeg, destinationPeg, originPeg) //Moving n and other n-1 disks to the final destination
      }
    }

    loop(n, 1, 2, 3)
  }

  def hanoiStream(n: Int): Unit = {

    def loop(n: Int, from: Int, to: Int, via: Int): Stream[String] = {
      if (n == 1) Stream(s"Move disk from pole $from to pole $to")
      else {
        println(s"$n pins left")
        loop(n - 1, from, via, to) #::: loop(1, from, to, via) #::: loop(n - 1, via, to, from)
      }
    }

    val movementStream = loop(n, 1, 2, 3)
    movementStream.take(8).toList.foreach(println)
  }

  hanoiTailRecursive(2)

}
