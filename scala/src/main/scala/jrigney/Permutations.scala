package jrigney

object Permutations extends App {

  //Think about taking the first digit and finding all the permutations of the remaining digits
  //do that for each digit in the array.
  def permsNonTailRecursive(xs: List[Int]): List[List[Int]] = {

    def loop(xs: List[Int]): List[List[Int]] = {

      //   println(s"xs [${xs}]")

      xs match {
        case Nil => List(Nil)
        case xs => {
          for {
            (x, i) <- xs.zipWithIndex //for each digit in xs
            restOfTheList = xs.take(i) ++ xs.drop(1 + i) // get the numbers except the one at i
            otherPermutations <- loop(restOfTheList) // get permutations the rest of the numbers except x
          } yield {
            x :: otherPermutations // add the selected back onto permutations
          }
        }
      }
    }

    loop(xs)
  }

  //  println(s"permsNonTailRecursive ${permsNonTailRecursive(List(1,2,3))}")


  def permsCool(s: List[Int]): List[List[Int]] =
    if (s.size == 1) List(s)
    else s.flatMap { c =>
      val permOtherDigits = permsCool(s.filterNot(_ == c))
      permOtherDigits.map(c +: _)
    }

  //    println(s"permsCool ${permsCool(List(1,2,3))}")

}

