package jrigney

import org.scalatest.{Matchers, WordSpec}

class BfsMatrixTest extends WordSpec with Matchers {

  "BFS & DFS for an UnweightedGraph using an adjacency matrix" should {

    "for graph1 and starting at 0 return the vertices from the start outward" in {
      val expected = List(0, 1, 2, 3)
      BfsMatrix.breadthFirstSearch(0, BfsMatrixTest.vectorVectorGraph1) should equal(expected)
    }

    "for graph3 and starting at 0 return the vertices from the start outward" in {
      val expected = List(0, 1, 5, 2, 3, 6, 4)
      BfsMatrix.breadthFirstSearch(0, BfsMatrixTest.vectorVectorGraph3) should equal(expected)
    }
  }
}
object BfsMatrixTest {

  val vectorVectorGraph1: BfsMatrix.MatrixGraph[Int] = Vector(
    Vector(0, 1, 1, 0),
    Vector(0, 0, 1, 0),
    Vector(0, 0, 0, 1),
    Vector(0, 0, 0, 1))

  val vectorVectorGraph2: BfsMatrix.MatrixGraph[Int] = Vector(
    Vector(2, 3),
    Vector(6),
    Vector(2),
    Vector.empty,
    Vector(6, 7),
    Vector(1),
    Vector.empty,
    Vector.empty)

  val vectorVectorGraph3: BfsMatrix.MatrixGraph[Int] = Vector(
    Vector(0, 1, 0, 0, 0, 1, 0), //0
    Vector(0, 0, 1, 1, 0, 0, 0), //1
    Vector(0, 0, 0, 0, 0, 0, 0), //2
    Vector(0, 0, 0, 0, 1, 0, 0), //3
    Vector(0, 0, 0, 0, 0, 0, 0), //4
    Vector(0, 0, 0, 0, 0, 0, 1), //5
    Vector(0, 0, 0, 0, 0, 0, 0) //6
  )
}
