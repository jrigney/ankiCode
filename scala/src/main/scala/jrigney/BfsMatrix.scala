package jrigney

import scala.annotation.tailrec

object BfsMatrix {
  type MatrixGraph[Vertex] = Vector[Vector[Vertex]]

  //Write a breadth first search using adjacency matrix
  def breadthFirstSearch(startVertex: Int, graph: MatrixGraph[Int]): List[Int] = {

    @tailrec
    def loop(fringe: List[Int], result: List[Int], visited: List[Int]): List[Int] = {
      fringe match {
        case Nil => result.reverse
        case head :: rest =>
          val potentialNeighbors = graph(head).zipWithIndex.filter { case (v, _) => v == 1 }.map { _._2 }
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)

          val newResult = if (visited.contains(head)) result else head +: result

          //          println(s"f $fringe r $result v $visited pN $potentialNeighbors n $newNeighbors")
          if (newNeighbors.isEmpty) loop(rest, newResult, head +: visited)
          else loop(rest ++ newNeighbors, newResult, head +: visited)
      }
    }

    loop(List(startVertex), List.empty, List.empty)

  }
}
