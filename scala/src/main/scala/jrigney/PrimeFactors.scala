package jrigney

import scala.annotation.tailrec

object PrimeFactors extends App {

  def factorize(x: Int): List[Int] = {

    //base case: numberToFindFactorsFor is prime
    //progress case: if numberToFindFactorsFor can be divided by guessedFactor then add guessedFactor to acc and
    // loop with factor that is not prime(numberToFindFactorsFor/guessedFactor)
    //else get new guessedFactor (guessedFactor + 1) and try again on the same numberToFindFactorsFor

    //basically this is pulling out the primes and getting the next non-prime factor and factorizing that
    /**
      *       12
      *      /  \
      *     2    6
      *        /   \
      *       2     3
      *
      * The tree structure suggests recursive
      */

    @tailrec
    def loop(numberToFindFactorsFor: Int, guessedFactor: Int, acc: List[Int]): List[Int] = {

      println(s"numberToFindFactorsFor [${numberToFindFactorsFor}] guessedFactor [${guessedFactor}] acc [${acc}]")

      val guessedFactorIsFactorOf_? = numberToFindFactorsFor % guessedFactor == 0


      guessedFactor * guessedFactor > numberToFindFactorsFor match {

        //guessedFactor is a factor so loop again with the result of numberToFindFactorsFor / guessedFactor as the new
        // number we need factors for.
        case false if guessedFactorIsFactorOf_? => loop(
          numberToFindFactorsFor / guessedFactor,
          guessedFactor,
          guessedFactor +: acc)

        //guessedFactor is not a factor so try another number as the factor
        case false => loop(numberToFindFactorsFor, guessedFactor + 1, acc)

        case true => numberToFindFactorsFor +: acc
      }

    }

    loop(x, 2, List.empty)
  }

  println(s"prime factors ${PrimeFactors.factorize(12)}")

}
