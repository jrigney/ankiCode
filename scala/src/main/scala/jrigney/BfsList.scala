package jrigney

import scala.annotation.tailrec
import scala.collection.immutable.HashMap

object BfsList {
  type ListGraph[Vertex] = HashMap[Vertex, Vector[Vertex]]

  def breadthFirstSearch[V](startVertex: V, graph: ListGraph[V]): List[V] = {

    @tailrec
    def loop(fringe: List[V], result: List[V], visited: List[V]): List[V] = {

      fringe match {
        case Nil => result.reverse
        case head :: rest =>
          val potentialNeighbors = fringe.flatMap(graph)
          val unvisitedNeighbors = potentialNeighbors.filterNot(visited.contains)

          val newResult = if (visited.contains(head)) result else head +: result

//          println(s" head [$head] rest [$rest] result [$result] visited [$visited] \n\tpN [$potentialNeighbors \n\tnN [$unvisitedNeighbors]\n\tnR [$newResult]")

          if (unvisitedNeighbors.isEmpty) loop(rest, newResult, head +: visited)
          else loop(rest ++ unvisitedNeighbors, newResult, head +: visited)
      }
    }

    loop(List(startVertex), List.empty, List.empty)
  }
}
