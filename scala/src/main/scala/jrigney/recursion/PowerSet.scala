package jrigney.recursion

object PowerSet extends App {
  /**
    * The idea is to think about how to build the next set based on the previous set.
    * However you are given an entire set so you must also think about how to break that down.
    *
    * Given set [] return []
    * Given set [1] return [] [1]
    * Given set [1,2] return [] [1] [2] [1,2]
    * Given set [1,2,3] return [] [1] [2] [3] [1,2] [1,3] [2,3] [1,2,3]
    *
    * Notice that next set is made up of the previous set and the previous set combined with the next element of the set
    * Look at the sets above broken down differently
    *
    * Given set [1,2] return [] [1] [2] [1,2]
    * Given set [1,2,3] return from [1,2] -> [] [1] [2] [1,2]
    *                         addition of 3 [3] [1,3] [2,3] [1,2,3]
    *
    * See how 3 +: [], 3 +: [1], 3 +: [2], 3 +: [1,2]
    *
    *
    *
    */
  def power[A](t: Set[A]): Set[Set[A]] = {

    @annotation.tailrec
    def pwr(t: Set[A], acc: Set[Set[A]]): Set[Set[A]] =

      if (t.isEmpty) acc //base case
      else {

        val previousSet = acc //[] [1] [2] [1,2]
        val setCombinedWithNext = acc map (_ + t.head) //[3] [1,3] [2,3] [1,2,3]

        pwr(t.tail, previousSet ++ setCombinedWithNext)
      }

    pwr(t, Set(Set.empty[A])) //Powerset of ∅ is {∅}
  }

  println(s"powerSet ${power(Set(1, 2, 3))}")
}
