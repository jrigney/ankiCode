package jrigney

object Nqueen extends App {

  type Queen = (Int, Int)
  type Solutions = List[List[Queen]]

  val size = 4

  def placeQueens(n: Int): Solutions = {

    n match {
      case 0 => List(Nil)
      case _ =>
        val x = for {
          queens <- placeQueens(n - 1)
          _ = println(s"queens [${queens}] n [${n}]")
          y <- 1 to size
          queen = (n, y)
          _ = println(s"queen [${queen}]")
          if (safe_?(queen, queens))
        } yield queen :: queens
        println(s"\t n [${n}] x.size [${x.size}] x [${x}]")
        x
    }

  }

  def safe_?(queen: Queen, others: List[Queen]): Boolean = {
    val x = others forall (!attackable_?(queen, _))
    println(s"\tsafe? queen [${queen}] x $x")
    x
  }

  def attackable_?(q1: Queen, q2: Queen): Boolean =
    q1._1 == q2._1 || //same rows
      q1._2 == q2._2 || //same columns
      (q2._1 - q1._1).abs == (q2._2 - q1._2).abs //diagnals


  val solutions = placeQueens(size)
  println(solutions.size + " solutions found")
  // print the board of the first solution
  for (queen <- solutions.head; x <- 1 to size) {
    if (queen._2 == x) print("Q ") else print(". ")
    if (x == size) println()
  }
}
