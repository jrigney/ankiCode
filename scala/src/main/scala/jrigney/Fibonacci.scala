package jrigney

import scala.annotation.tailrec

object Fibonacci extends App {

  /** Using dynamic programming.  Only the two previous numbers need to be kept not every calculated number needs to be kept
    * Notice that we start at the fib we want (5) and make it mean the remaining sums we need
    * and we start the calculations from 0 since we know that fib 0 is 0
    *
    * f0 f1 f2 f3 f4 f5
    * 0  1  1  2  3  5
    */
  def fib(n: Int): Int = {

    @tailrec
    def loop(remainingSumsToCalculate: Int, nMinus2: Int, nMinus1: Int): Int ={

//      println(s"remainingSumsToCalculate [$remainingSumsToCalculate] nMinus2 [$nMinus2] nMinus1 [$nMinus1]")

      remainingSumsToCalculate match {
        case 0 => nMinus2
        case 1 => nMinus1
        case _ => loop(remainingSumsToCalculate - 1, nMinus1, nMinus2 + nMinus1) //
      }

    }
    loop(n, 0, 1)  //we embed the knowledge of fib 0 and fib 1
  }

  println(s"fib ${fib(5)}")
}
