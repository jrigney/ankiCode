package jrigney


object TypeclassExample {

  //Define the typeclass
  import annotation.implicitNotFound
  @implicitNotFound("No member of type class NumberLike in scope for ${T}")
  trait NumberLike[T] {
    def plus(x: T, y: T): T

    def divide(x: T, y: Int): T

    def minus(x: T, y: T): T
  }

  //Create default implementations in the companion object of the typeclass trait
  object NumberLike {

    //Make note of the implicit.  This is important to make the typeclass available
    implicit object NumberLikeDouble extends NumberLike[Double] {
      def plus(x: Double, y: Double): Double = x + y

      def divide(x: Double, y: Int): Double = x / y

      def minus(x: Double, y: Double): Double = x - y
    }

    implicit object NumberLikeInt extends NumberLike[Int] {
      def plus(x: Int, y: Int): Int = x + y

      def divide(x: Int, y: Int): Int = x / y

      def minus(x: Int, y: Int): Int = x - y
    }

  }

}

object TypeclassUsageExample {
  import jrigney.TypeclassExample._

  /** Notice that ev works more like a function that takes variables of a type that has a typeclass instance
    * and not as a method on the type.
    */
  def mean[T](xs: Vector[T])(implicit ev: NumberLike[T]): T = ev.divide(
    xs.reduce( ev.plus(_, _)),
    xs.size)

  /**
    * Notice the [T:NumberLike] context bound.  It is synactic sugar for the implicit above.
    * Notice the implicitly[NumberLike[T]] is used in place of the ev used above.  The context bound NumberLike does
    * not have the implicit parameter anymore so you can't use ev.  Instead you use implicitly[NumberLike[T]]
    */
  def meanContextBound[T: NumberLike](xs: Vector[T]): T =
  implicitly[NumberLike[T]].divide( xs.reduce( implicitly[NumberLike[T]].plus(_, _)), xs.size)



}

