package jrigney

object IsPrime extends App {
  def isPrime(n: Int): Boolean = {

    n match {
      case x if x < 1 => false
      case 1 => false
      case 2 => true
      case x if x % 2 == 0 => false
      case x =>
        val range = 3 until x - 1 by 2
        !range.exists(x => n % x == 0)
    }
  }

    (1 to 20).foreach(i => if (isPrime(i)) println("%d is prime.".format(i)))
//  println(s"isPrime ${isPrime(3)}")
}
