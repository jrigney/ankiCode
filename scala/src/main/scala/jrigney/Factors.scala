package jrigney

import scala.annotation.tailrec

object Factors extends App {

  private def primeFactors(x: Int): List[Int] = {

    @tailrec
    def loop(remaining: Int, guessedFactor: Int, acc: List[Int]): List[Int] = {

      //      println(s"remaining [${remaining}] guessedFactor [${guessedFactor}] acc [${acc}]")

      guessedFactor * guessedFactor > remaining match {
        case false if remaining % guessedFactor == 0 => loop(remaining / guessedFactor, guessedFactor, guessedFactor :: acc)
        case false => loop(remaining, guessedFactor + 1, acc)
        case true => remaining :: acc
      }

    }

    loop(x, 2, List.empty)
  }

  // x = 28
  //Get the prime factors for x -- 7,2,2
  //Get all ways to combine prime factors -- (7), (2), (7,2), (2,2)
  //Get the product of each combination -- 7, 2, 14, 4
  //Concat with the original prime factors -- 7,2,14,4,7,2,2
  //Concat back 1 and x since those make up the factors - 1,28,7,2,14,4,7,2,2
  //Remove duplicates -- 1,28,7,2,14,4
  def factors(x: Int): List[Int] = {
    val primes = primeFactors(x)
    val combinations = (1 until primes.size).flatMap(i => primes.combinations(i).toList)
    val factors = (combinations.map(_.product).toList ++ primes)
    (List(1, x) ++ factors ).distinct
  }

  println(s"${factors(28).sorted}")
}
