package jrigney

import org.scalatest.{FunSuite, Matchers}

class VectorOpsTest  extends FunSuite with Matchers {

  test("identify indicies with a value of 1") {
    val fn: ((Int,Int)) => Boolean = {case (value,_) => value == 1}
    val actual = VectorOps.extractIndicices(Vector(1,0,1,1), fn )
    val expected = Vector(0,2,3)
    actual shouldBe expected
  }
}
