package jrigney

import org.scalatest.{FunSuite, Matchers}

class FibonacciTest extends FunSuite with Matchers {
  test("fibonacci") {
    Fibonacci.fib(35) shouldBe(9227465)
  }
}
