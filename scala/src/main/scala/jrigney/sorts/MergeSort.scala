package jrigney.sorts

import scala.annotation.tailrec

object MergeSort extends App {

  /**
    *  1 split the array at the mid point
    *  2 mergeSort the left
    *  3 mergeSort the right
    *  4 merge the two halves
    *
    *  the tricks are merging the two side and the base case
    *  merging is looking at the head of left and head of right and put the lower one on the accumulator
    *  the base case is when there is only one element in the list.
    */
  def mergeSort(xs: List[Int]): List[Int] = {

    @tailrec
    def merge(l:List[Int], r: List[Int], acc:List[Int]): List[Int] = {
      println(s"l [${l}] r [${r}] acc [${acc}]")
      (l,r) match {
        case (_,Nil) => acc ++ l //base case
        case (Nil,_) => acc ++ r //base case
        case (l +: ls,r +: rs) =>
          if (l < r) merge(ls, r +:rs, acc :+ l)
          else merge(l +: ls, rs, acc :+ r)
      }
    }

    val mid = xs.length/2
    if (mid == 0) xs
    else {
      val (l,r) = xs.splitAt(mid) //split the array at the midpoint
      val lSorted = mergeSort(l) //mergeSort the left
      val rSorted = mergeSort(r) //mergeSort the right
      merge(lSorted, rSorted, List.empty) //merge the left and right
    }
  }

  println(s"mergeSort ${mergeSort(List(2,7,5,4))}")
}
