package jrigney

import org.scalatest.{FunSuite, Matchers}

class IsPrimeTest extends FunSuite with Matchers {
  test("is prime") {

    IsPrime.isPrime(5) shouldBe true
    IsPrime.isPrime(4) shouldBe false

  }
}
