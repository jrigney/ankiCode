package jrigney

object VectorOps {

  def extractIndicices[A](xs: Vector[A], fn: ((A,Int)) => Boolean):Vector[Int] = {
    xs.zipWithIndex.filter(fn).map((_._2))
  }
}
