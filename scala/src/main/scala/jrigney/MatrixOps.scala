package jrigney

object MatrixOps {

 def isDiagonal(position1: (Int,Int), position2: (Int,Int)): Boolean = {
   (position1._1 - position2._1).abs == (position1._2 - position2._2).abs
 }
}
