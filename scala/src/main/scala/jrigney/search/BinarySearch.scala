package jrigney.search

import scala.annotation.tailrec

object BinarySearch extends App {

  def binarySearchTailRecursive(list: Vector[Int], target: Int): Boolean = {

    @tailrec
    def loop(list: Vector[Int]): Boolean = {

      val mid = list.length / 2

      //      println(s"list $list mid [${mid}] midValue [${list(mid)}]")
      list match {
        case l if l.isEmpty => false
        case l if l.length == 1 => if (l(mid) == target) true else false

        case l if l(mid) == target => true

        case l if target > l(mid) =>
          val (_, restExcludeMid) = l.splitAt(mid)
          loop(restExcludeMid)

        case l if target < l(mid) =>
          val (restIncludeMid, _) = l.splitAt(mid)
          loop(restIncludeMid)

      }
    }

    list.sorted //binary search requires sorted
    loop(list)

  }

  //  println(binarySearchTailRecursive(Vector(1, 2, 3, 4, 5, 6), 5))
  //  println(binarySearchTailRecursive(Vector(1, 2, 3, 4, 5, 6), 1))
  //  println(binarySearchTailRecursive(Vector(1, 2, 3, 4, 5, 6), 11))
  //  println(binarySearchTailRecursive(Vector(1, 2, 3, 4, 5), 4))

  def binarySearchRecursiveIndex(list: Vector[Int], target: Int): Option[Int] = {

    type Index = Int

    @tailrec
    def loop(start: Int, end: Int): Option[Index] = {
      if (start > end) None
      else {
        val mid = start + (end - start + 1) / 2

//        println(s"start [${start}] end [${end}] mid [${mid}]")
        if (list(mid) == target)
          Some(mid)
        else if (target < list(mid))
          loop(start, mid - 1) // left side
        else
          loop(mid + 1, end) // right side

      }
    }

    loop(list.indexOf(list.head), list.indexOf(list.last))
  }

//  println(binarySearchRecursiveIndex(Vector(1, 2, 3, 4, 5, 6), 5))
//  println(binarySearchRecursiveIndex(Vector(1, 2, 3, 4, 5, 6), 1))
//  println(binarySearchRecursiveIndex(Vector(1, 2, 3, 4, 5, 6), 11))
//  println(binarySearchRecursiveIndex(Vector(1, 2, 3, 4, 5), 4))
    println(binarySearchRecursiveIndex(Vector(0,1,2,3,4,5,6,7,8,9), 7))

}
