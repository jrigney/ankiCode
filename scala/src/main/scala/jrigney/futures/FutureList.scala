package jrigney.futures

import java.util.concurrent.{CountDownLatch, TimeUnit}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object FutureList extends App {

  import scala.concurrent.ExecutionContext.Implicits.global


  def futureToFutureTry[T](f: Future[T]): Future[Try[T]] =
    f.map(x => Success(x)).recover { case e => Failure(e) }

  def futureOf[A, B[_]](fs: List[Future[A]], futureFailuresOf: Future[A] => Future[B[_]]): Future[List[B[_]]] = {
    val listOfFuture = fs.map(futureFailuresOf(_))
    Future.sequence(listOfFuture)
  }

  def fOf[A](fs:List[Future[A]]) = {
    Future.traverse(fs)(futureToFutureTry)
  }


  val listOfFutures = Future.successful(1) :: Future.failed(new Exception("Failure")) ::
    Future.successful(3) :: Nil
  val ls = futureOf(listOfFutures, futureToFutureTry[Int])
  val futureListOfSuccesses = ls.map(_.filter(_.isSuccess))

  val latch = new CountDownLatch(1)
  futureListOfSuccesses.onComplete { x =>
    x match {
      case Success(l) => println(s"Success $l")
      case Failure(e) => println(s"failure $e")
    }

    latch.countDown()
  }
  latch.await(1, TimeUnit.SECONDS)
  //  println(s"futureOf ${ls} futureListSuccess ${futureListOfSuccesses}")


  //  def allSuccessful[A, M[X] <: TraversableOnce[X]](in: M[Future[A]])
  //                                                  (implicit cbf: CanBuildFrom[M[Future[A]], A, M[A]],
  //                                                   executor: ExecutionContext): Future[M[A]] = {
  //    in.foldLeft(Future.successful(cbf(in))) {
  //      (fr, fa) ⇒ (for (r ← fr; a ← fa) yield r += a) fallbackTo fr
  //    } map (_.result())
  //  }
}
