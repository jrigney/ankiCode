package jrigney

object Prime extends App {
  def is(i: Long) =
    if (i == 2) true
    else if ((i & 1) == 0) false // efficient div by 2
    else prime(i)

  def primes: Stream[Long] = 2 #:: prime3

  private val prime3: Stream[Long] = {
    @annotation.tailrec
    def nextPrime(i: Long): Long =
      if (prime(i)) i else nextPrime(i + 2) // tail

    def next(i: Long): Stream[Long] =
      i #:: next(nextPrime(i + 2))

    3 #:: next(5)

  }

  // assumes not even, check evenness before calling - perf note: must pass partially applied >= method
  def prime(i: Long): Boolean =
    prime3 takeWhile (math.sqrt(i.toDouble).>= _) forall { i % _ != 0 }

  /* Prime.is is the prime check predicate,
   * and Prime.primes returns a Stream of all prime numbers.
   * prime3 is where the Stream is computed,
   * using the prime predicate to check for all prime divisors less than the
   * square root of i.
   */

  println(s"4 is prime? ${Prime.is(4)}")
  println(s"primes ${Prime.primes.takeWhile(_ < 6).toList}")
}