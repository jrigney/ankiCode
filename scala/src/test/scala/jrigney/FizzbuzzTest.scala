package jrigney

import org.scalatest.{FunSuite, Matchers}

class FizzbuzzTest extends FunSuite with Matchers {

  val expected = "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16"

  test("fizzbuzz1") {
    (1 to 16).map(FizzBuzz.fizzbuzz1).mkString(",") shouldBe expected
  }

  test("fizzbuzz2") {
    (1 to 16).map(FizzBuzz.fizzbuzz2).mkString(",") shouldBe expected
  }

  test("fizzbuzz3") {
    (1 to 16).map(FizzBuzz.fizzbuzz3).mkString(",") shouldBe expected
  }
}
