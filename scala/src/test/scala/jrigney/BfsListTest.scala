package jrigney

import org.scalatest.{Matchers, WordSpec}

import scala.collection.immutable.HashMap

class BfsListTest extends WordSpec with Matchers {

  "BFS for an UnweightedGraph using a adjacency list" should {
    "for graph1 return the vertices from the start outward" in {
      val expected = List[Int](0, 1, 2, 3)
      BfsList.breadthFirstSearch(0, BfsListTest.hashmapGraph1) should equal(expected)
    }

    "for graph2 and starting at 0 return the vertices from the start outward" in {
      val expected = List[Int](0, 2, 3, 4, 6, 7)
      BfsList.breadthFirstSearch(0, BfsListTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting at 1 return the vertices from the start outward" in {
      val expected = List[Int](1, 6)
      BfsList.breadthFirstSearch(1, BfsListTest.hashmapGraph2) should equal(expected)
    }
  }

}

object BfsListTest {
  /**
    * digraph graph1 {
    * 0 -> 1 -> 2 -> 3;
    * 0 -> 2;
    * 2 -> 0;
    * 3 -> 3;
    * }
    */
  val hashmapGraph1: BfsList.ListGraph[Int] = HashMap[Int, Vector[Int]](
    (0 -> Vector[Int](1, 2)),
    (1 -> Vector[Int](2)),
    (2 -> Vector[Int](0, 3)),
    (3 -> Vector[Int](3)))


  /**
    * digraph graph2 {
    * 0 -> 2 -> 4 -> 7
    * 0 -> 3
    * 1 -> 6
    * 4 -> 6
    * }
    */

  val hashmapGraph2: BfsList.ListGraph[Int] = HashMap[Int, Vector[Int]](
    (0 -> Vector[Int](2, 3)),
    (1 -> Vector[Int](6)),
    (2 -> Vector[Int](4)),
    (3 -> Vector.empty[Int]),
    (4 -> Vector[Int](6, 7)),
    (5 -> Vector[Int](1)),
    (6 -> Vector.empty[Int]),
    (7 -> Vector.empty[Int]))

  val hashmapGraph3: BfsList.ListGraph[Int] = HashMap[Int, Vector[Int]](
    (1 -> Vector[Int](2, 6)),
    (2 -> Vector[Int](1)),
    (3 -> Vector[Int](1, 5)),
    (4 -> Vector[Int](7)),
    (5 -> Vector[Int](1, 6)),
    (6 -> Vector[Int](2, 4, 5)),
    (7 -> Vector[Int](6)))

}
