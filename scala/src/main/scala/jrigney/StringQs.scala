package jrigney

object StringQs extends App {

  def words(x: String): Array[String] = {
    x.split(" ").filterNot(_ == "")
  }
//  println(words("alpha bravo     charlie").toList)

  def indexOfLetterOfSecond(x: String): Int = {
    //get index of letter after index 6
    x.indexOf("a", 6)
  }
//  println(indexOfLetterOfSecond("hhontahhacc"))


  def duplicateLetters(x:String): List[Char] ={
    val grouped = x.groupBy(identity)

    val dups = grouped.flatMap {
      case (c, xs) if (xs.size > 1) => Some(c)
      case _ => None
    }

    dups.toList
  }

//  println(s"duplicateLetters ${duplicateLetters("Java")}")

  def anagrams_?(x:String, y:String): Boolean = {
    x.toLowerCase.sorted == y.toLowerCase.sorted
  }
//  println(anagrams_?("stop", "post"))

  def firstNonRepeated(x:String): Option[ Char  ] = {
    val charToCount = x.groupBy(identity)

    x.collectFirst{case c if charToCount(c).size == 1 => c}
  }
//  println(s"${firstNonRepeated("acabdabac")}")

  def reverseString(x:String): String = {
    x.foldLeft(""){(acc, next) => next +: acc}
  }
//  println(s"reverseString ${reverseString("alpha")}")
}
