package jrigney

import org.scalatest.{FunSuite, Matchers}

class PrimeFactorsTest extends FunSuite with Matchers {

  test("prime factors") {
    val actual = PrimeFactors.factorize(12)
    actual.sorted shouldBe List(2,2,3)
    actual.product shouldBe 12
  }
}
