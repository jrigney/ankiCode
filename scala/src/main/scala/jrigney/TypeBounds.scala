package jrigney

object UpperBounds {

  //Given the following what types can be instantiated?
  class Parking[A <: Vehicle]

  val vehicle: Parking[Vehicle] = new Parking[Vehicle]

  val car: Parking[Car] = new Parking[Car]

  val jeep: Parking[Jeep] = new Parking[Jeep]

  val motorcycle: Parking[Motorcycle] = new Parking[Motorcycle]

  val smallJeep = new Parking[SmallJeep]

  //This can not be instantiated because <: is an upper bound.
  //Meaning Parking[A <: Vehicle] can only take a type parameter that is of type Vehicle or a subtype of Vehicle
  //val thing: Parking[Thing] = new Parking[Thing]

}

object LowerBounds {

  //Given the following what types can be instantiated?
  class Parking[A >: Jeep]

  val jeep = new Parking[Jeep]

  val car: Parking[Car] = new Parking[Car]

  val vehicle = new Parking[Vehicle]

  //This can not be instantiated because >: is a lower bound.
  //Meaning Parking[A >: Jeep] can only take a type parameter that is of type Jeep or a supertype of Jeep
  //val smallJeep = new Parking[SmallJeep]
}

trait Thing

trait Vehicle extends Thing

class Car extends Vehicle

class Jeep extends Car

class SmallJeep extends Jeep

class Coupe extends Car

class Motorcycle extends Vehicle

class Vegetable


//class Parking[A](val place: A)
