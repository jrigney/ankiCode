package jrigney

// https://apiumhub.com/tech-blog-barcelona/scala-generics-covariance-contravariance/
object Variance {


  /**
    * The code below fails to compile because it would be possible to do the following
    *
    *  val pets: Pets[Animal] = new Pets[Cat](new Cat)
    *  pets.add(Dog())
    *
    *  in the example above pets looks like it is type Pets[Animal] but it is actually Pets[Cat].
    *  the Pets[+A] below says you can instantiate Pets with A or a subclass of A.  That mean the [+A] only indicate what
    *  you can use not what it is.  So Pets[Animal] indicate what can be used.  Pets[Cats](new Cat) is what the actual
    *  type is.
    *  The A in pet2:A dictates the type must match the instantiated type which is Cat in this example.
    *
    *  Error:(20, 11) covariant type A occurs in contravariant position in type A of value pet2
    */
  //class Pets[+A](val pet:A) {
  //  def add(pet2: A): String = "done"
  //}


  /**
    * The code below fails to compile because it would be possible to do the following
    *
    *  val pets: Pets[Cat] = new Pets[Animal](new Animal)
    *
    *  The compiler would expect pets.pet to be Cat but pets.pet is not Cat, it is an Animal.
    *  Even though Pets[-A] is contravariant pet:A is not and once the type is defined as in
    *  val pets: Pets[Cat] the type is fixed
    *
    *  If the type was covariant as in Pets[+A] it would work because the type would be pets:Pets[Cat]
    *
    * Error: contravariant type A occurs in covariant position in type => A of value pet
    * class Pets[-A](val pet:A)
    */
  //class Pets[-A](val pet:A) {
  //}
  class Pets[+A](val pet:A)
  val pets: Pets[Animal] = new Pets[Cat](new Cat)

}
trait Animal

class Cat() extends Animal

class Dog() extends Animal

class SubCat extends Cat
